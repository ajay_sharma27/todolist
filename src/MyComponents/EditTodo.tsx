import { useState } from 'react';
export const EditTodo = (props: any) => {
    const [title, setTitle] = useState(props.currTitle);
    const [desc, setDesc] = useState(props.currDescription);
    const submit = (e: any) => {
        e.preventDefault();
        if (!title || !desc) {
            alert("Title or Description cannot be blank");
        }
        else {
            props.editTodo(title, desc);
            setTitle("");
            setDesc("");
        }
    }
    return (
        <div className="container my-3">
            <h3>Edit a Todo</h3>
            <form onSubmit={submit}>
                <div className="mb-3">
                    <label htmlFor="title" className="form-label">Todo Title</label>
                    <input type="text" onChange={(e) => { setTitle(e.target.value) }} className="form-control" id="title" aria-describedby="emailHelp" value={title} />
                </div>
                <div className="mb-3">
                    <label htmlFor="desc" className="form-label">Todo Desription</label>
                    <input type="text" onChange={(e) => { setDesc(e.target.value) }} className="form-control" id="desc" value={desc} />
                </div>
                <button type="submit" className="btn btn-sm btn-success">Add Todo</button>
            </form>
        </div>
    )
}
