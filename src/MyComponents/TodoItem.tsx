export const TodoItem = (props: any) => {
    return (
        <>
            <div>
                <h4>{props.todo.title}</h4>
                <p>{props.todo.desc}</p>
                <button className="btn btn-sm btn-danger" onClick={() => { props.onDelete(props.todo) }}>Delete</button>{' '}
                <button className="btn btn-sm btn-warning" onClick={() => { props.onEdit(props.todo) }}>Edit</button>
            </div><hr />
        </>
    )
}
