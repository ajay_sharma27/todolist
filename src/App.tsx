import './App.css';
import Header from "./MyComponents/Header";
import { Todos } from "./MyComponents/Todos";
import { Footer } from "./MyComponents/Footer";
import { AddTodo } from "./MyComponents/AddTodo";
import { useState, useEffect } from 'react';
import { EditTodo } from './MyComponents/EditTodo';
function App() {
  let initTodo = [] as any;
  let userJson = localStorage.getItem('todos');
  if (userJson === null) {
    initTodo = [];
  }
  else {
    initTodo = JSON.parse(userJson);
    // some other ways to initialize initTodo
    // initTodo = JSON.parse(localStorage.getItem("todos") || '{}');
    // const temp = localStorage.getItem('todos');
    // initTodo = JSON.parse(JSON.stringify(localStorage.getItem('todos')));
    // const userJson = localStorage.getItem('todos');
    // initTodo = userJson !== null ? JSON.parse(userJson) : new User();
  }
  const onDelete = (todo: any) => {
    setTodos(todos.filter((e: any) => {
      return e !== todo;
    }));
    localStorage.setItem("todos", JSON.stringify(todos));
  }
  const onEdit = (todo: any) => {
    let sno = 0;
    // eslint-disable-next-line array-callback-return
    todos.filter((e: any) => {
      if (e === todo) {       
        setCurrIndex(sno);
        setType(false);
        setDescription(todo.desc);
        setTitle(todo.title);
      }
      sno = sno + 1;
    });
  }
  const addTodo = (title: any, desc: any) => {
    let sno;
    if (todos.length === 0) {
      sno = 0;
    }
    else {
      sno = todos[todos.length - 1].sno + 1;
    }
    const myTodo = {
      sno: sno,
      title: title,
      desc: desc,
    }
    setTodos([...todos, myTodo]);
  }
  const editTodo = (title: any, desc: any) => {
    const myTodo = {
      sno: currIndex,
      title: title,
      desc: desc,
    }
    todos[currIndex] = myTodo;
    setTodos([...todos]);
  }
  const [todos, setTodos] = useState(initTodo);
  const [currIndex, setCurrIndex] = useState(0);
  const [currType, setType] = useState(true);
  const [currTitle, setTitle] = useState("");
  const [currDescription, setDescription] = useState("");
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos])
  return (
    <>
      <Header title="MyTodosList" searchBar={false} />
      {
        currType ? <AddTodo addTodo={addTodo} /> : <EditTodo editTodo={editTodo} currTitle={currTitle} currDescription={currDescription} />
      }
      <Todos todos={todos} key={todos.sno} onDelete={onDelete} onEdit={onEdit} />
      <Footer />
    </>
  );
}
export default App;
